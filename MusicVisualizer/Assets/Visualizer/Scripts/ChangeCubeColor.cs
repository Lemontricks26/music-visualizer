﻿using UnityEngine;
using System.Collections;

public class ChangeCubeColor : MonoBehaviour {
    
    public Material[] materials;
    public float changeInterval = 0.3f;
    public Renderer rend;
    
    void Start () {
        rend = GetComponent<Renderer>();
        rend.enabled = true;
    }
	
	void Update () {
        
        int index = Mathf.FloorToInt(Time.time / changeInterval);

        index = index % materials.Length;

        rend.sharedMaterial = materials[index];
    }
}
