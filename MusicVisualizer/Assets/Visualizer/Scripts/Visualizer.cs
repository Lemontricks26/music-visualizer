﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Visualizer : MonoBehaviour {
    
    private AudioSource aSource;
    public float[] samples = new float[64];
    private LineRenderer lineRenderer;
    public GameObject cubes;
    public GameObject plane;
    private Transform goTransform;
    private Vector3 cubePos;
    private Transform[] cubesTransform;
    public GameObject cubesTwo;
    public Vector3 gravity = new Vector3(0.0f, 0.25f, 0.0f);
    
    List<GameObject> cubesScaleTwo;

    public Material[] materials;
    public float changeInterval = 0.3f;
    public Renderer rend;

    public Text pitch;
    public Text volume;
    public Text audioLength;
    private int length;

    private int minutes;
    private int seconds;

    public float position = 1;

    private bool playing = true;
    private bool off = false;

    void Awake()
    { 
        this.aSource = GetComponent<AudioSource>();
        this.lineRenderer = GetComponent<LineRenderer>();
        this.goTransform = GetComponent<Transform>();
        rend = GetComponent<Renderer>();
        rend.enabled = true;
    }

    void Start()
    {
        lineRenderer.SetVertexCount(samples.Length);
        cubesTransform = new Transform[samples.Length];
        goTransform.position = new Vector3(-samples.Length / position, goTransform.position.y, goTransform.position.z);
        GameObject tempCube;
        cubesScaleTwo = new List<GameObject>();

        for (int i = 0; i < samples.Length; i++)
        {
            tempCube = (GameObject)Instantiate(cubes, new Vector3(goTransform.position.x + i, goTransform.position.y, goTransform.position.z), Quaternion.identity);
            cubesTransform[i] = tempCube.GetComponent<Transform>();
            cubesTransform[i].parent = goTransform;

            GameObject obj = (GameObject)Instantiate(cubesTwo, new Vector3(-200 + i, 138, -94), transform.rotation);
            obj.SetActive(false);
            cubesScaleTwo.Add(obj);
        }
        plane.SetActive(false);
    }

    void Update()
    {
        aSource.GetSpectrumData(this.samples, 0, FFTWindow.BlackmanHarris);

        for (int i = 0; i < samples.Length; i++)
        {
            cubePos.Set(cubesTransform[i].position.x, Mathf.Clamp(samples[i] * (100 + i * i), 0, 300), cubesTransform[i].position.z);
            cubesScaleTwo[i].transform.localScale = new Vector3(1, samples[i] * 1200, 1);
            int index = Mathf.FloorToInt(Time.time / changeInterval);
            index = index % materials.Length;
            cubesScaleTwo[i].GetComponent<Renderer>().sharedMaterial = materials[index];

            if (cubePos.y >= cubesTransform[i].position.y)
                cubesTransform[i].position = cubePos;
            else
                cubesTransform[i].position -= gravity;

            lineRenderer.SetPosition(i, cubePos - goTransform.position);
        }

        minutes = (int)aSource.GetComponent<AudioSource>().time/60;
        seconds = (int)aSource.GetComponent<AudioSource>().time%60;

        pitch.text = aSource.pitch.ToString();
        volume.text = aSource.volume.ToString();
        audioLength.text = minutes.ToString("00") + ":" + seconds.ToString("00");

        if (Input.GetKeyDown("q"))
        {

            if (off == true)
            {
                for (int i = 0; i < samples.Length; i++)
                {
                    cubesScaleTwo[i].SetActive(false);
                }
                plane.SetActive(false);
                off = false;
            }
            else if (off == false)
            {
                for (int i = 0; i < samples.Length; i++)
                {
                    cubesScaleTwo[i].SetActive(true);
                }
                plane.SetActive(true);
                off = true;
            }
        }

        if (Input.GetKeyDown("w"))
            aSource.pitch += 0.1f;

        if (Input.GetKeyDown("s"))
            aSource.pitch -= 0.1f;

        if (Input.GetKeyDown("a"))
            aSource.volume -= 0.1f;

        if (Input.GetKeyDown("d"))
            aSource.volume += 0.1f;

        int indexTwo = Mathf.FloorToInt(Time.time / changeInterval);
        indexTwo = indexTwo % materials.Length;
        rend.sharedMaterial = materials[indexTwo];
    }
    
    void FixedUpdate()
    {
         if (Input.GetKeyDown("space"))
         {
            if (playing == true)
            {
                playing = false;
                aSource.Pause();
            }
            else if (playing == false)
            {
                playing = true;
                aSource.UnPause();
            }
         }

        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }
}