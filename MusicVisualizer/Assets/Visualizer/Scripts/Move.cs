﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {

    private bool direction = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(this.gameObject.transform.position.x == 100)
        {
            direction = false;
        }
        else if(this.gameObject.transform.position.x == -100)
        {
            direction = true;
        }

        if (direction == true)
        {
            this.gameObject.transform.position += new Vector3(1.0f, 0, 0);
        }
        else if(direction == false)
        {
            this.gameObject.transform.position += new Vector3(-1.0f, 0, 0);
        }
    }
}
